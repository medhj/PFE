package com.example.webmethodsanalyze;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



public class StepsCommentsCheck {
	
	private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());
	
	public static List<String> TagsList = new ArrayList<String>() {{
		add("MAPDELETE");
	    add("MAPCOPY");
	    add("MAPTARGET");
	    add("MAPSOURCE");
	    add("MAPSET");
	}};
	
	public static Boolean checkCommentsNeeded(Node node,String fileName) {
		
		
		int depth=calculateDepth(node, 0);
		int comment=CheckComments(node ,0);
		if(depth>=6) {
			
			logger.warning("The flow Service "+fileName+" has "+depth+" level of depth");
			if(comment<(depth/2)) {
				logger.warning("You need to add more comments to the flow Service "+fileName+"");
			}
			return false;
		}
		else {
			return true;
		}
		
	
		
	}
	
	public static int CheckComments(Node node,int commentsnumber) {
		
		if (node == null) {
            return commentsnumber;
        }
        
		int maxDepth = commentsnumber;
		
		if (node.getNodeType() == Node.ELEMENT_NODE) {
        	
	           
        	if(node.getNodeName().equals("COMMENT")) {
        		if(!node.getTextContent().equals("")&& node.getTextContent()!=null) {
        			maxDepth+=1;
        		}
        	}
            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
            	maxDepth=CheckComments(children.item(i),maxDepth);
            }
        }
		return maxDepth;
    }
	
	
	
	
	
	private static int calculateDepth(Node node, int currentDepth) {
        
		 if (node == null) {
	            return currentDepth;
	        }
	        

	        int maxDepth = currentDepth;
	        NodeList children = node.getChildNodes();
	        
	        for (int i = 0; i < children.getLength(); i++) {
	            Node child = children.item(i);
	            if (child.getNodeType() == Node.ELEMENT_NODE   ) {
	            	if(!TagsList.contains(child.getNodeName())) {
	            		int childDepth = calculateDepth(child, currentDepth + 1);
	                if (childDepth > maxDepth) {
	                    maxDepth = childDepth;
	                }
	            	}
	                
	            }
	        }

	        return maxDepth;
	    }

}
