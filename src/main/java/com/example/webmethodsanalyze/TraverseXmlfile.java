package com.example.webmethodsanalyze;

import java.io.FileInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TraverseXmlfile {
	
	public static void parsXmlFile(String Pathfile) {
		try {
            // Chemin du fichier XML
			
            // Charger le document XML à partir du fichier
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new FileInputStream(Pathfile));

            // Normaliser le document
            doc.getDocumentElement().normalize();

            // Démarrer la traversée à partir de l'élément racine
            //traverseAndPrint(doc.getDocumentElement());
            traverseAndPrint(doc.getDocumentElement());
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public static void traverseAndPrint(Node node) {
        if (node.getNodeType() == Node.ELEMENT_NODE) {
        	if(node.getNodeName().equals("INVOKE")) {
        		System.out.println("--------------------------RMA-------------------");
        		NamedNodeMap map=node.getAttributes();
        		/*for(int i=0; i<map.getLength();i++) {
        			System.out.println(map.item(i).getNodeName());
        		}*/
        		Node node1=map.item(0);
    			System.out.println(node1.getNodeValue());
        		
        		System.out.println("--------------------------RMA-------------------");
        	}
            //System.out.println(node.getNodeName());
            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                traverseAndPrint(children.item(i));
                //if(children.item(i).getNodeName().equals("Values")) System.out.println("-------------------------rdr-------------");
            }
        }
    }
	
	public static String checkfiletype(String Pathfile) {
		try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new FileInputStream(Pathfile));

            doc.getDocumentElement().normalize();
            Node node=doc.getDocumentElement();
            return traverse(node);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
		return "";
	}
	
	public static String traverse(Node node) {
		if (node.getNodeType() == Node.ELEMENT_NODE) {
        	if(node.getNodeName().equals("Values")) {
        		Node node1=node.getFirstChild().getNextSibling();
        		/*for(int i=0; i<map.getLength();i++) {
        			System.out.println(map.item(i).getNodeValue());
        			return node.getTextContent();
        		}*/
    			//System.out.println(node1.getTextContent());

        		return node1.getTextContent();
        		

        	}
            
        }
		return "";
	}
	
	public static Node getNodefromPath(String Pathfile) {
		try {

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new FileInputStream(Pathfile));

            doc.getDocumentElement().normalize();
            return doc.getDocumentElement();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
		return null;
		
	}
	
}
